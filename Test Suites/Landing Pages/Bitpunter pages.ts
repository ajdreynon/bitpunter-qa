<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Bitpunter pages</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>47eb7098-5d9a-4de5-ae80-65c737a30bd0</testSuiteGuid>
   <testCaseLink>
      <guid>5e50d71b-56fd-4357-a811-829e0757203c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT change page content language</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8d8dca7-e156-43e3-a1a3-f824efe7a9bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view Contact page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>319bec09-d513-466b-a274-a262a1ef5db9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09c04c9a-c3d4-43b3-bf5e-17fcef8ce542</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ_Affiliates tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dde65fb9-f6ef-4f06-9e73-1b9b56a855ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ_DepositWithdrawal tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea39d5cc-5623-4ff3-aced-451ae3eb15c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ_Fee tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>724fcc87-26a1-40d8-addb-72d1abb13293</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ_My Profile tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9f3fcf7-0ca2-4450-a376-fbde2f607d62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ_Trade tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94702668-bcad-4b09-8f23-a8a3f01a0a50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view FAQ_Transfer tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffddfbcd-0622-4128-96fa-5d75ee940e93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view Games tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a00619b3-4d3f-4db8-aaa8-026a848812ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view Gaming platform page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04be8ac5-7df2-4ab9-b9f3-84fbd475eb27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view Information_News and Media page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d1506fb-2526-46c2-a11a-d866c06f369e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view Information_Road Map page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89c938fb-1f23-4860-9511-3ca2edcdaa08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view landing page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b37e87-54f4-4b4d-bf75-878ab9b4a533</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Landing page/As a Guest ISBAT view Transmining page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
