<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Validation</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8deda362-9c72-4ef5-8e11-6e34187d3134</testSuiteGuid>
   <testCaseLink>
      <guid>d3281a20-d33a-4847-bba6-dd680385f997</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify if a user can login with alphanumeric character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04e7768c-2ade-4d8b-b6f2-be1726fed90b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify if a user cannot enter the characters more than the specified range in each field Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51d5a900-3c49-4e9d-81be-1027f9b7d77b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify if a user cannot enter the characters more than the specified range in each field Username</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c64586d1-c710-4167-b206-0b7ee739df72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify if a user cannot login with a valid username and an invalid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1c22997-3ae7-445e-bbb6-cb61f4ea5b99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify if a user will be able to login with a valid username and valid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>824a8d7c-7ead-43cb-837c-b6bc10fc3595</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify if the user cannot Login with Invalid username and valid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8be1656-56c4-4f79-9203-1b290ba62561</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify the Forgot Password functionality</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c82895a-042d-4d9f-91b9-158e529445d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify the login page for both, when the field is blank and Submit button is clicked</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccca0754-5efd-4206-993c-0db89a4754bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify the login page, Enter invalid Email Address</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dbc90bb-99fe-4164-b522-4c5abee68261</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Login/Verify the messages for invalid login</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
