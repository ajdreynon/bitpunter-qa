<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sign Up</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2da0042b-26e6-4a0c-87f0-ba0f0dc141ad</testSuiteGuid>
   <testCaseLink>
      <guid>286506d3-73f3-4c3a-a99d-465025bcea5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISBAT register account (no affiliates)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0a47da4-29e6-42bb-92fa-62b8b3ea46b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISBAT register account (with affiliates)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf01dbcd-9e6a-465e-81b1-9c1641d8cf40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISBAT register with numeric name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55eae8d7-e89a-428f-8a7d-fa9ada543055</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISBAT view registration form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d86a6b76-8381-4de4-85ea-ad13bcee5e41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register invalid email format</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdf895ba-b410-42b8-8b2e-e8b8c6a56651</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with blank fields and checked terms and conditions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c875ceb5-edf1-4545-be1e-530e52a10f76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with blank fields with uncheck terms and conditions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59913b87-db3c-4ad3-9fb7-8a6061dcf839</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with email already taken</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1dcfb82-3b16-4694-af6a-1a1855019273</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with mismatched password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f748dda-ed8c-4d3e-9d3c-bb08ffb9b766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with name using special characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4736e110-1681-43df-b92d-e3a4b57cba15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with password characters less than the minimum</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20c8ed05-306d-4f79-9205-263b696fc4f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with password characters more than the minimum required</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63b9998a-c4bd-40c7-912d-d79dbc6c7cf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bitpunter Registration/As a User ISNBAT register with unchecked terms and conditions</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
