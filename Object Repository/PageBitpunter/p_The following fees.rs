<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_The following fees</name>
   <tag></tag>
   <elementGuidId>34b04856-f1c0-41e7-8f13-64220f2bdeaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='pills-fee']/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The following fees are applicable on Bitpunter.io:
            
                
                
                    TYPE
                    FEES
                
                
                    SEND – Cryptocurrency To External E-Wallets
                    Network Fees
                
                
                RECEIVE – Cryptocurrency From External E-Wallets
                FREE
            
            
                    TRANSFER – Bitpunter.io to Bitpunter.com Gaming Wallet
                    FREE
                
                
                    TRANSFER – Bitpunter.com Gaming Wallet to Bitpunter.io
                    FREE
                
                
                    TRADE – Cryptocurrency To Cryptocurrency
                    0.8%
                
                
                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pills-fee&quot;)/div[1]/p[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='pills-fee']/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What Fees Are Charged On Bitpunter.io?'])[1]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='When Will My Tokens Be Awarded?'])[1]/following::p[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/p</value>
   </webElementXpaths>
</WebElementEntity>
