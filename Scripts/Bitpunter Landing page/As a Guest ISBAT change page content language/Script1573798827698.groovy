import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://v2.bitpunter.io/')

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_zh'))

String validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com是世界一流的線上體育博彩和娛樂場所， 根據遊戲牌照365 / JAZ，由古拉梳政府發放許可並受其監管。 交易開採在加密貨幣行業創造了范式轉變，並推動了幾個月的新交易，在交易量方面已超過了既定市場領導者。', validateText, true)) {
    WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_zh_tw'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com是世界一流的線上體育博彩和娛樂場所， 根據遊戲牌照365 / JAZ，由古拉梳政府發放許可並受其監管。 交易開採在加密貨幣行業創造了范式轉變，並推動了幾個月的新交易，在交易量方面已超過了既定市場領導者。', validateText, true)) {
	WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_es'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com es una casa de apuestas en línea de clase mundial y casino con licencia y regulada por el Gobierno de Curacao bajo licencia de juego 365/JAZ. Transaction Mining ha creado un cambio de paradigma en la industria de criptomonedas y catapultado nuevos intercambios que tienen unos pocos meses por encima de los líderes del mercado establecidos en términos de volumen de transacciones.', validateText, true)) {
	WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_vn'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com là nhà cái và sòng bài cá cược thể thao trực tuyến đẳng cấp thế giới được cấp phép và giám sát bởi Chính phủ Curacao theo giấy phép cờ bạc 365/JAZ. Khai thác dựa trên giao dịch đã tạo ra sự thay đổi mô hình trong ngành công nghiệp tiền điện tử và tạo nên các sàn giao dịch mới ít hơn vài tháng tuổi so với các nhà lãnh đạo thị trường đã gầy dựng được khối lượng giao dịch.', validateText, true)) {
	WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_th'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com เป็น หนังสือ กีฬา และ คา สิ โน ออนไลน์ ระดับ โลก ที่ ได้ รับ อนุญาต และ ควบคุม โดย รัฐบาล ของ Curacao ภาย ใต้ ใบ อนุญาต เล่น เกม 365 / JAZ  ธุรกรรม การ ขุด ได้ ส มีอายุ ไม่ กี่ เดือน ข้าง ต้น ผู้นำ ตลาด ที่ จัดตั้ง ขึ้น ใน แง่ ของ ปริมาณ การ ทำ ธุรกรรม', validateText, true)) {
	WebUI.comment('Failed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_jp'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.comは、ゲーミングライセンス365 / JAZの下で、立憲君主制によって認可および規制されている世界クラスのオンラインスポーツブック・カジノです。取り引きのマイニングは、暗号通貨業界にパラダイムシフトを生み出し、マーケットリーダーよりも数か月前に取引量の面で確立された新しい取引所を切り開きました。', validateText, true)) {
	WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_kr'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com은 쿠라카오 정부가 게임 라이선스 365/JAZ로 규제하는 세계적인 온라인 스포츠북 및 카지노입니다. 거래 마이닝은 암호화된 통화 산업에 패러다임의 변화를 만들어 냈고, 거래 규모 면에서 기존 시장 선도자들보다 몇 달 더 많은 새로운 교환을 촉진시키고 있습니다.', validateText, true)) {
	WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_hi'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com गेमिंग लाइसेंस 365 / JAZ के तहत कुराकाओ सरकार द्वारा लाइसेंस प्राप्त और विनियमित एक विश्व स्तरीय ऑनलाइन स्पोर्ट्सबुक और कैसीनो है। ट्रांसजेक्शन माइनिंग ने क्रिप्टोकरंसी उद्योग में एक प्रतिमान बदलाव पैदा किया है और ट्रांसजेक्शन की मात्रा के मामले में स्थापित बाजार के लीडरों के सामने कुछ महीने पहले के नए एक्सचेंजों की पेशकश की है।', validateText, true)) {
	WebUI.comment('Passed')
}

WebUI.click(findTestObject('PageBitpunter/div_English'))

WebUI.click(findTestObject('PageBitpunter/lang_en'))

validateText = WebUI.getText(findTestObject('PageBitpunter/p_intro_1'))

if (WebUI.verifyMatch('Bitpunter.com is a world class online sportsbook and casino licensed and regulated by the Government of Curacao under gaming license 365/JAZ. Transaction Mining has created a paradigm shift in the cryptocurrency industry and catapulted new exchanges that are a few months old above established market leaders in terms of transaction volume.', validateText, true)) {
	WebUI.comment('Passed')
}

