import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('v2.bitpunter.io')

WebUI.click(findTestObject('Registration elements/button_Register'))

WebUI.verifyElementVisible(findTestObject('Registration elements/div_Sign Up For Free'))

WebUI.sendKeys(findTestObject('Registration elements/input_Sign Up For Free_name'), 'John Doe')

if (WebUI.verifyElementVisible(findTestObject('Registration elements/div_Sign Up For Free'))) 
{
    int randomInt
    randomInt = ((Math.random() * 10000) as int)
    WebUI.sendKeys(findTestObject('Registration elements/input_Sign Up For Free_email'), ('user' + randomInt) + '@qatest.com')
}

WebUI.sendKeys(findTestObject('Registration elements/input_Sign Up For Free_password'), 'password')

WebUI.sendKeys(findTestObject('Registration elements/input_Sign Up For Free_password_confirmation'), 'password')

WebUI.sendKeys(findTestObject('Registration elements/input_Sign Up For Free_aff_code'), 'Source')

WebUI.check(findTestObject('Registration elements/input_Mandatory fields_inlineCheckbox1'))

WebUI.verifyElementChecked(findTestObject('Registration elements/input_Mandatory fields_inlineCheckbox1'), 1)

WebUI.click(findTestObject('Registration elements/span_Register'))

WebUI.verifyElementVisible(findTestObject('Registration elements/div_dashboard'))

